#!/bin/bash

# TODO use getopt and whatever, at least it works

UCHROMEFILE=$1
# echo $UCHROMEFILE
# if [[ ! -f $UCHROMEFILE ]]; then (echo -e "Provide a path to a \033[1muserChrome.css\033[0m file"; exit 1) fi

firefox --new-tab about:support >> /dev/null || echo -e "Open \033[1mabout:support\033[0m in Firefox"
while
    echo -e "Enter the \033[1mProfile Directory\033[0m path:" && read PROFILEPATH
    [[ ! -d $PROFILEPATH || ! -f $PROFILEPATH/signedInUser.json ]]
do true; done

mkdir $PROFILEPATH/chrome/
cp $UCHROMEFILE $PROFILEPATH/chrome/userChrome.css
echo "Installed userChrome.css, restart Firefox for effect"